package com.gogive.dashboard.helpers;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Database {

    final private String user;
    final private String url;
    final private String password;

    public Database(){
        Properties prop = new Properties();
        String fileName = "database.properties";

        InputStream is = FileResourcesUtils.getFileFromResourceAsStream(fileName);
        try {
            prop.load(is);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.url = prop.getProperty("url");
        this.user = prop.getProperty("username");
        this.password = prop.getProperty("password");
    }

    public Database(String url , String user , String password) {
        this.user = user;
        this.url = url;
        this.password = password;
    }

    /**
     * Connect to a PostgreSQL database.
     * @param url the JDBC URL to connect to. Must start with "jdbc:postgresql:"
     * @param user the username for the connection
     * @param password the password for the connection
     * @return a connection object for the established connection
     * @throws ClassNotFoundException if the driver class cannot be found on the Java class path
     * @throws java.sql.SQLException if the connection to the database fails
     */
    public java.sql.Connection connect()
            throws ClassNotFoundException, java.sql.SQLException
    {
        /*
         * Register the PostgreSQL JDBC driver.
         * This may throw a ClassNotFoundException.
         */
        Class.forName("org.postgresql.Driver");
        java.util.Properties props = new java.util.Properties();
        props.setProperty("user", this.user);
        props.setProperty("password", this.password);
        /* don't use server prepared statements */
        props.setProperty("prepareThreshold", "0");
        /*
         * Tell the driver manager to connect to the database specified with the URL.
         * This may throw an SQLException.
         */
        return java.sql.DriverManager.getConnection(this.url, props);
    }
}
