package com.gogive.dashboard.helpers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class ScreenController {
    public void newWindow(String viewName, Class<?> elClass) {
        Parent root;
        try {
            root = FXMLLoader.load(Objects.requireNonNull(elClass.getResource(viewName+".fxml")));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}