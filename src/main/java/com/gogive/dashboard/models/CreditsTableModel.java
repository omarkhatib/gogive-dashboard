package com.gogive.dashboard.models;

public class CreditsTableModel {
    int id;
    int credits;
    String email;

    public CreditsTableModel(int credits, String email) {
        this.credits = credits;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
