package com.gogive.dashboard.models;

public class UserTableModel {
    int id;
    String fname,lname,email,pcode,usertype;

    public UserTableModel(int id, String fname, String lname, String email, String pcode, String usertype) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.pcode = pcode;
        this.usertype = usertype;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }
}
