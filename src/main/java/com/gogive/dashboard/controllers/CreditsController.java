package com.gogive.dashboard.controllers;

import com.gogive.dashboard.helpers.Database;
import com.gogive.dashboard.models.CreditsTableModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.sql.SQLException;

public class CreditsController {
    @FXML
    private TableView<CreditsTableModel> credit_table;
    @FXML
    private TableColumn<CreditsTableModel, Integer> col_credits_credit;
    @FXML
    private TableColumn<CreditsTableModel, String> col_credits_email;

    ObservableList<CreditsTableModel> oblist = FXCollections.observableArrayList();

    public void initialize() {
        try {
            Database db = new Database();
            var conn = db.connect();
            var st = conn.createStatement();
            String query = String.format("SELECT email,credits FROM credits left join (select id,email from users) as users on users.id = credits.user_id");
            var res = st.executeQuery(query);
            while(res.next()) {
                String email = res.getString("email");
                int credits = res.getInt("credits");
                oblist.add(new CreditsTableModel(credits,email));
            }
            col_credits_credit.setCellValueFactory(new PropertyValueFactory<>("id"));
            col_credits_email.setCellValueFactory(new PropertyValueFactory<>("email"));

            credit_table.setItems(oblist);
        } catch (ClassNotFoundException | SQLException e) {
            new Alert(Alert.AlertType.ERROR , e.getMessage()).show();
            return;
        }
    }
}