package com.gogive.dashboard.controllers;

import com.gogive.dashboard.helpers.ScreenController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class AnalyticController {

    @FXML
    protected void ontopdonatorButtonClick(ActionEvent event) {
        new ScreenController().newWindow("topdonator-view",getClass());
    }

    @FXML
    protected void onmostrevealedButtonClick(ActionEvent event) {
        new ScreenController().newWindow("mostrevealed-view",getClass());
    }
}