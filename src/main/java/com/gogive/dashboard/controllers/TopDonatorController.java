package com.gogive.dashboard.controllers;

import com.gogive.dashboard.helpers.Database;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TopDonatorController {
    @FXML
    private Label period_donator_id;
    @FXML
    private Label donatorname_id;
    @FXML
    protected void onlastweekDButtonClick() {
        topdonator(  7);
    }
    @FXML
    protected void onlastmonthDButtonClick() {
        topdonator( 30);
    }
    @FXML
    protected void onlast3monthsDButtonClick() {
        topdonator( 90);
    }
    public  void topdonator( int p)
    {
        String periode,Uname="";
        try {
            if(p==7)
                periode="1 Week";
            else if(p==30)
                periode="1 Month";
            else periode ="3 Months";
            Database db = new Database();
            var conn = db.connect();
            var st = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            String query = String.format("SELECT user_id,firstname,lastname,count(*) as cnt\n" +
                    "FROM items left join users on user_id = users.id \n" +
                    "WHERE item_type = 'donation' and items.created_at > 'now'::timestamp - '"+periode+"'::interval\n" +
                    "group by user_id,firstname,lastname\n" +
                    "ORDER BY cnt DESC\n" +
                    "LIMIT 1");
            var res = st.executeQuery(query);
            if(res.first())
            {
                 Uname = res.getString("firstname");
                 Uname+=" "+res.getString("lastname");
            } else {
                Uname = "No Result";
            }

            period_donator_id.setText(periode);
            donatorname_id.setText(Uname);

        } catch (ClassNotFoundException | SQLException e) {
            new Alert(Alert.AlertType.ERROR , e.getMessage()).show();
            return;
        }
    }
}
