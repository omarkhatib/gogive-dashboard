package com.gogive.dashboard.controllers;

import com.gogive.dashboard.helpers.Database;
import com.gogive.dashboard.models.UserTableModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.sql.SQLException;

public class UsersTableController {
    @FXML
    private TableView<UserTableModel> user_table;
    @FXML
    private TableColumn<UserTableModel, Integer> col_id;
    @FXML
    private TableColumn<UserTableModel, Integer> col_fname;
    @FXML
    private TableColumn<UserTableModel, Integer> col_lname;
    @FXML
    private TableColumn<UserTableModel, Integer> col_email;
    @FXML
    private TableColumn<UserTableModel, Integer> col_pcode;
    @FXML
    private TableColumn<UserTableModel, Integer> col_usertype;

    ObservableList<UserTableModel> oblist = FXCollections.observableArrayList();

    public void initialize() {
        try {
            Database db = new Database();
            var conn = db.connect();
            var st = conn.createStatement();
            String query = String.format("SELECT id,firstname,lastname,email,pcode,usertype FROM users");
            var res = st.executeQuery(query);
            while(res.next()) {
                int id = res.getInt("id");
                String fname = res.getString("firstname");
                String lname = res.getString("lastname");
                String email = res.getString("email");
                String pcode = res.getString("pcode");
                String usertype = res.getString("usertype");
                oblist.add(new UserTableModel(id,fname,lname,email,pcode,usertype));
            }
            col_id.setCellValueFactory(new PropertyValueFactory<>("id"));
            col_fname.setCellValueFactory(new PropertyValueFactory<>("fname"));
            col_lname.setCellValueFactory(new PropertyValueFactory<>("lname"));
            col_email.setCellValueFactory(new PropertyValueFactory<>("email"));
            col_pcode.setCellValueFactory(new PropertyValueFactory<>("pcode"));
            col_usertype.setCellValueFactory(new PropertyValueFactory<>("usertype"));

            user_table.setItems(oblist);
        } catch (ClassNotFoundException | SQLException e) {
            new Alert(Alert.AlertType.ERROR , e.getMessage()).show();
            return;
        }
    }
}

