package com.gogive.dashboard.controllers;

import com.gogive.dashboard.helpers.BCrypt;
import com.gogive.dashboard.helpers.Database;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;

public class LoginController {
    @FXML
    private TextField emailField;
    @FXML
    private PasswordField passwordField;

    private Parent root;

    @FXML
    protected void onLoginButtonClick(ActionEvent event) {
        String email = emailField.getText();
        String password = passwordField.getText();
        String password_hash = "";

        if (email.equals("") || password.equals("")) {
            new Alert(Alert.AlertType.ERROR , "please provide email and password").show();
            return;
        }
        String usertype = "";

        try {
            Database db = new Database();
            var conn = db.connect();
            var st = conn.createStatement();
            String query = String.format("SELECT id,firstname,lastname,email,encode(password_hash, 'escape') as password_hash,usertype FROM users WHERE email='%s'",email);
            var res = st.executeQuery(query);
            if(res.next()) {
                usertype = res.getString("usertype");
                password_hash = res.getString("password_hash");
            } else {
                new Alert(Alert.AlertType.ERROR , "User name or password are wrong").show();
                return;
            }
        } catch (ClassNotFoundException | SQLException e) {
            new Alert(Alert.AlertType.ERROR , e.getMessage()).show();
            return;
        }

        if (BCrypt.checkpw(password , password_hash)) {
            if(usertype.equals("admin")) {
                try {
                    root = FXMLLoader.load(
                            Objects.requireNonNull(
                                    getClass().getResource("admin-view.fxml")
                            )
                    );
                } catch (IOException e) {
                    new Alert(Alert.AlertType.ERROR , e.getMessage()).show();
                }
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
            } else  if(usertype.equals("analytic")) {
                try {
                    root = FXMLLoader.load(
                            Objects.requireNonNull(
                                    getClass().getResource("analytic-view.fxml")
                            )
                    );
                } catch (IOException e) {
                    new Alert(Alert.AlertType.ERROR , e.getMessage()).show();
                }
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
            }
            else {
                new Alert(Alert.AlertType.ERROR , "You don't have permission to use this app").show();
            }
        } else {
            new Alert(Alert.AlertType.ERROR , "User name or password are wrong").show();
        }
    }
}