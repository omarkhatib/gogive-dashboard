package com.gogive.dashboard.controllers;

import com.gogive.dashboard.helpers.ScreenController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class AdminController {
    @FXML
    private void onUsersBtnClick() {
        new ScreenController().newWindow("users-view" , getClass());
    }

    @FXML
    protected void onGenTokenButtonClick(ActionEvent event) {
        new ScreenController().newWindow("gentoken-view",getClass());
    }

    @FXML
    protected void onMetricsButtonClick(ActionEvent event) {
        new ScreenController().newWindow("metrics-view",getClass());
    }

    @FXML
    protected void onCreditsButtonClick(ActionEvent event) {
        new ScreenController().newWindow("credits-view",getClass());
    }
}
