package com.gogive.dashboard.controllers;

import com.gogive.dashboard.helpers.JsonReader;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.util.Duration;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Timer;


public class MetricsController {

    @FXML
    private LineChart<Integer,Integer> metrics_tpt;
    @FXML
    private LineChart<Integer,Integer> metrics_trr;
    @FXML
    private LineChart<Integer,Integer> metrics_trs;
    @FXML
    private PieChart metrics_rs;
    @FXML
    private Label mem_stats;
    @FXML
    private Label res1;
    @FXML
    private Label res2;
    @FXML
    private Label res3;
    @FXML
    private Label res4;
    @FXML
    private Label res5;
    @FXML
    private Label res6;
    @FXML
    private Label res7;
    @FXML
    private Label res8;
    @FXML
    private Label res9;

    private int currX = 0;

    private final XYChart.Series<Integer, Integer> metrics_tpt_data = new XYChart.Series<Integer , Integer>();
    private final XYChart.Series<Integer, Integer> metrics_trr_data = new XYChart.Series<Integer , Integer>();
    private final XYChart.Series<Integer, Integer> metrics_trs_data = new XYChart.Series<Integer , Integer>();
    private final ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

    public void initialize() throws IOException {
        pieChartData.add(new PieChart.Data("200" , 0));
        pieChartData.add(new PieChart.Data("404" , 0));
        metrics_rs.setData(pieChartData);

        metrics_tpt_data.getData().add(new XYChart.Data<>(currX,0));

        metrics_tpt.getData().add(metrics_tpt_data);
        metrics_tpt.setCreateSymbols(false);
        metrics_tpt.setLegendVisible(false);

        metrics_trr_data.getData().add(new XYChart.Data<>(currX,0));
        metrics_trr.getData().add(metrics_trr_data);
        metrics_trr.setCreateSymbols(false);
        metrics_trr.setLegendVisible(false);

        metrics_trs_data.getData().add(new XYChart.Data<>(currX,0));
        metrics_trs.getData().add(metrics_trs_data);
        metrics_trs.setCreateSymbols(false);
        metrics_trs.setLegendVisible(false);

        currX++;

        Timer t = new Timer();

        Timeline fiveSecondsWonder = new Timeline(
                new KeyFrame(Duration.seconds(1),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                JSONObject res = null;
                                try {
                                    res = JsonReader.readJsonFromUrl("http://localhost:4000/debug/vars");
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                final int total_requests_received = res.getInt("total_requests_received");
                                final int total_processing_time = res.getInt("total_processing_time_μs");
                                final int total_responses_sent = res.getInt("total_responses_sent");
                                final JSONObject total_responses_sent_by_status = res.getJSONObject("total_responses_sent_by_status");
                                final JSONObject memory_stats = res.getJSONObject("database");

                                currX++;
                                updateRsPieChart(total_responses_sent_by_status);
                                updateTPTChart(total_processing_time);
                                updateTRRChart(total_requests_received);
                                updateTRSChart(total_responses_sent);
                                updateMemoryData(memory_stats);
                            }
                        }));
        fiveSecondsWonder.setCycleCount(Timeline.INDEFINITE);
        fiveSecondsWonder.play();
    }

    public void updateRsPieChart(JSONObject vals) {
        try {
            pieChartData.get(0).setPieValue(vals.getInt("200"));
        } catch (Exception ignored) {
        }
        try {
            pieChartData.get(1).setPieValue(vals.getInt("404"));
        } catch (Exception ignored) {
        }
    }

    public void updateTPTChart(int val) {
        metrics_tpt_data.getData().add(new XYChart.Data<>(currX,val));
    }

    public void updateTRRChart(int val) {
        metrics_trr_data.getData().add(new XYChart.Data<>(currX,val));
    }

    public void updateTRSChart(int val) {
        metrics_trs_data.getData().add(new XYChart.Data<>(currX,val));
    }

    public void updateMemoryData(JSONObject memory_stats) {
        res1.setText(Integer.toString(memory_stats.getInt("MaxOpenConnections")));
        res2.setText(Integer.toString(memory_stats.getInt("OpenConnections")));
        res3.setText(Integer.toString(memory_stats.getInt("InUse")));
        res4.setText(Integer.toString(memory_stats.getInt("Idle")));
        res5.setText(Integer.toString(memory_stats.getInt("WaitCount")));
        res6.setText(Integer.toString(memory_stats.getInt("WaitCount")));
        res7.setText(Integer.toString(memory_stats.getInt("WaitCount")));
        res8.setText(Integer.toString(memory_stats.getInt("WaitCount")));
        res9.setText(Integer.toString(memory_stats.getInt("WaitCount")));
    }
}
