package com.gogive.dashboard.controllers;

import com.gogive.dashboard.helpers.Database;
import com.gogive.dashboard.models.UserTableModel;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MostRevealedController {
    @FXML
    private Label period_item_id;
    @FXML
    private Label itemname_id;
    @FXML
    private Label numberoftime_id;
    @FXML
    protected void onlastweekButtonClick() {
         mostrevealedItem(7);
    }
    @FXML
    protected void onlastmonthButtonClick() {
        mostrevealedItem( 30);
    }
    @FXML
    protected void onlast3monthsButtonClick() {
        mostrevealedItem(90);
    }
    public  void mostrevealedItem( int p)
    {
        String periode,Iname="",nbre="";
        try {
            Database db = new Database();
            var conn = db.connect();
            var st = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            String query = String.format("SELECT items.name, count(*) as cnt\n" +
                    "\tFROM reveals left join items on reveals.item_id = items.id\n" +
                    "\tWHERE revealed_at > 'now'::timestamp - '1 week'::interval\n" +
                    "\tGROUP BY items.name\n" +
                    "\tORDER BY cnt DESC\n" +
                    "\tLIMIT 1");
            var res = st.executeQuery(query);
            if(res.first())
            {
                 Iname = res.getString("name");
                 nbre=Integer.toString(res.getInt("cnt"));
            } else {
                Iname = "No Result";
                nbre = "0";
            }
            if(p==7)
               periode="Week";
            else if(p==30)
                periode="Month";
            else periode ="3 Months";
            period_item_id.setText(periode);
            itemname_id.setText(Iname);
            numberoftime_id.setText(nbre);

        } catch (ClassNotFoundException | SQLException e) {
            new Alert(Alert.AlertType.ERROR , e.getMessage()).show();
            return;
        }
    }
}
