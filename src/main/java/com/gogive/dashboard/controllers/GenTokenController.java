package com.gogive.dashboard.controllers;

import com.gogive.dashboard.helpers.Database;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.apache.commons.codec.binary.Base32;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;


public class GenTokenController {
    @FXML
    private TextArea token_text_area;
    @FXML
    private TextField token_user_id;

    @FXML
    protected void onGenTokenButtonClick() {
        if(token_user_id.getText().equals("")) {
            Alert a = new Alert(Alert.AlertType.ERROR , "should not be empty");
            a.show();
            return;
        } else if (!isInteger(token_user_id.getText())) {
            Alert a = new Alert(Alert.AlertType.ERROR , "should be an integer");
            a.show();
            return;
        }

        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[16];
        random.nextBytes(bytes);

        Base32 base32 = new Base32();
        String token = base32.encodeAsString(bytes).replaceAll("=" , "");
        String hash = sha256(token);

        try {
            Database db = new Database();
            var conn = db.connect();
            int id = 0;
            try{
                id = Integer.parseInt(token_user_id.getText());
            }
            catch (NumberFormatException ex){
                ex.printStackTrace();
            }

            LocalDateTime tomorrow = LocalDateTime.now().plusDays(1);
            Timestamp timestamp = Timestamp.valueOf(tomorrow);
            String sqlTimestampInsertStatement = "INSERT INTO tokens (hash, user_id, expiry, scope)\n" +
                    "        VALUES (convert_to(?, 'LATIN1'), ?, ?, ?)";
            PreparedStatement preparedStatement = conn.prepareStatement(sqlTimestampInsertStatement);
            preparedStatement.setString(1, hash);
            preparedStatement.setInt(2 , id);
            preparedStatement.setTimestamp(3, timestamp);
            preparedStatement.setString(4, "authentication");
            int affected = preparedStatement.executeUpdate();
            preparedStatement.close();
            if (affected != 1) {
                new Alert(Alert.AlertType.ERROR , "Failed to generate token").show();
                return;
            }
        } catch (ClassNotFoundException | SQLException e) {
            new Alert(Alert.AlertType.ERROR , e.getMessage()).show();
            return;
        }

        token_text_area.setText(token);
    }

    public static String sha256(final String base) {
        try{
            final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(base.getBytes(StandardCharsets.UTF_8));
            final StringBuilder hexString = new StringBuilder();
            for (byte b : hash) {
                final String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    public static boolean isInteger(String s) {
        return isInteger(s,10);
    }

    public static boolean isInteger(String s, int radix) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),radix) < 0) return false;
        }
        return true;
    }
}
